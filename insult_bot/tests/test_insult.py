import unittest
from unittest.mock import Mock, patch, PropertyMock
from returns.io import _IOFailure

from insult_bot.insult import get_insult


class InsultTests(unittest.TestCase):
    @patch("insult_bot.insult._get_request")
    def test_get_insult__request_exception(self, mock_exception: Mock) -> None:
        set_mock_exception(ConnectionError("Mock connection error"), mock_exception)
        insult_response = get_insult()
        self.assertIsInstance(insult_response, _IOFailure)

    @patch("insult_bot.insult._get_request")
    def test_get_insult__result_not_json_exception(self, mock_insult: Mock) -> None:
        mock_insult.return_value = """{"insult_bot": ["value" }"""
        insult_response = get_insult()
        self.assertIsInstance(insult_response, _IOFailure)

    @patch("requests.Response.text", new_callable=PropertyMock)
    def test_get_insult__result_json_missing_insult_exception(
        self, mock_insult_response: Mock
    ) -> None:
        mock_insult_response.return_value = """{"foo": "bar" }"""
        insult_response = get_insult()
        self.assertIsInstance(insult_response, _IOFailure)


def set_mock_exception(exception: Exception, mock: Mock) -> Mock:
    mock.side_effect = exception
    return mock


if __name__ == "__main__":
    unittest.main()
