"""Insult people on slack!"""
from json import loads
from returns.io import IOResult, impure_safe
from requests import get, Response
from slack_sdk.web import SlackResponse
from slack.slack_client import send_message_to_user


def run_insult_bot(
    slack_token: str, channel_id: str
) -> IOResult[SlackResponse, Exception]:
    """Entry point for insult_bot bot"""
    return get_insult().bind(lambda x: send_message_to_user(x, channel_id, slack_token))


@impure_safe
def get_insult() -> str:
    """Fetch a random insult_bot"""
    url = "https://insult.mattbas.org/api/insult.json"
    response = _get_request(url)
    data = loads(response.text)
    insult = data["insult"]
    return insult


def _get_request(url: str) -> Response:
    with get(url) as response:
        return response
