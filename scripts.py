"""Run insult_bot bot"""
from insult_bot.insult import run_insult_bot
import logging
from os import getenv
from typing import List


def insult() -> None:
    """Entry point for insult_bot bot"""
    slack_token = getenv("SLACK_BOT_TOKEN", "--- TOKEN NOT SET ---")
    channel_ids = getenv("CHANNEL_IDS", "")
    member_ids = _parse_ids(channel_ids)
    for member_id in member_ids:
        logging.info(f"Sending insult_bot to ${member_id}...")
        run_insult_bot(slack_token, member_id).map(
            lambda slack_response: print(slack_response.data)
        ).unwrap()


def _parse_ids(id_csv: str) -> List[str]:
    if id_csv:
        return id_csv.split(",")
    return []
