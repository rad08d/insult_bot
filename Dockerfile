FROM python:3.9.4-buster

WORKDIR /app

COPY insult_bot/ ./insult_bot/
COPY ./slack/ ./slack/
COPY scripts.py .
COPY Makefile .
COPY pyproject.toml .

RUN pip install poetry
RUN make install-deps

CMD ["make", "run-insult-bot"]