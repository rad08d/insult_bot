# Insult Bot
Welcome to you one-stop shop for slack insults! 
Have a friend, or enemy, that you want to insult, but you don't want to get your hands dirty?
Use Insult Bot! Insult Bot is here to do your dirty work for you.

### Slack Workspace Setup
1) Enable your slack workspace to allow for this bot.
2) Save the slack auth token as you'll need it to run the bot.


### Requirements (Docker setup)
1) Docker installed on the local machine

### Requirements (Local machine setup)
1) Python 3.9
2) [Poetry](https://python-poetry.org/)

### Your First Insult
1) Provide a comma separated list of channel_ids to the `.env` file in the `CHANNEL_IDS` key value pair (specific users in slack also have their own channel id)
2) Provide the auth token generated at bot setup time in slack in the `.env` file in the `SLACK_BOT_TOKEN` key value pair
3) Build the insult bot container by running the following command: `make docker.build`
4) Run the following command: `make docker.run-insult-bot`