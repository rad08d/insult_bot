ifneq (,$(wildcard ./.env))
    include .env
    export
endif

IMAGE_NAME=insult-bot
IMAGE_TAG=latest

install-deps:
	poetry install

type-check:
	find . -type f -name "*.py" -exec poetry run mypy {} +

format-check:
	find . -type f -name "*.py" -exec poetry run black --check {} +

format:
	find . -type f -name "*.py" -exec poetry run black {} +

test:
	poetry run pytest -s

run-insult-bot:
	poetry run insult-bot

docker.build:
	docker build -t $(IMAGE_NAME):$(IMAGE_TAG) .

docker.run-insult-bot:
	docker run  \
		-e SLACK_BOT_TOKEN=${SLACK_BOT_TOKEN} \
		-e CHANNEL_IDS=${CHANNEL_IDS} \
		$(IMAGE_NAME):$(IMAGE_TAG)