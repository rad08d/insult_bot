import unittest
from unittest.mock import patch, Mock

from returns.io import _IOFailure
from slack_sdk.web import SlackResponse

from slack.slack_client import send_message_to_user


class MyTestCase(unittest.TestCase):
    @patch("slack.slack_client._send_message")
    def test_send_message_to_user__not_valid(self, mock_response: Mock):
        mock_response.return_value = SlackResponse(
            client=None,
            http_verb="",
            api_url="",
            req_args={},
            data={},
            headers={},
            status_code=500,
        )
        response = send_message_to_user("message", "fake_member_id", "fake_token")
        self.assertIsInstance(response, _IOFailure)


if __name__ == "__main__":
    unittest.main()
