"""This module is meant to serve as a wrapper around the slack sdk"""

from returns.io import impure_safe
from slack_sdk import WebClient as SlackWebClient
from slack_sdk.web import SlackResponse


@impure_safe
def send_message_to_user(
    message: str, member_id: str, slack_token: str
) -> SlackResponse:
    """Wrapper to send a slack message to a user"""
    response = _send_message(message, member_id, slack_token)
    return response.validate()


def _send_message(message: str, channel_id: str, slack_token: str) -> SlackResponse:
    client = _get_web_client(slack_token)
    return client.chat_postMessage(channel=channel_id, text=message)


def _get_web_client(token: str) -> SlackWebClient:
    return SlackWebClient(token)
